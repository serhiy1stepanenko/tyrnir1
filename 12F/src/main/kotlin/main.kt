import java.util.*


fun main(args: Array<String>) {

    val array = LongArray(10)

    val scanner = Scanner(System.`in`)
    fillAnArray(array, scanner.nextInt(), scanner.nextInt())

    changeElemsArray(array)
    for (i in array)
    {
        print(i)
        print(" ")
    }

}


private fun fillAnArray(distArr: LongArray, frtElem: Int, scdElem: Int) {
    if (distArr.size < 2) throw IllegalAccessException()
    if (frtElem == 0 && scdElem == 0) {
        Arrays.fill(distArr, 0)
        return
    }
    distArr[0] = frtElem.toLong()
    distArr[1] = scdElem.toLong()
    for (i in 2 until distArr.size) {
        distArr[i] = distArr[i - 1] + distArr[i - 2]
    }
}

private fun changeElemsArray(distArr: LongArray) {
    if (distArr.size < 2) return
    var tmp: Long
    for (i in 0 until distArr.size / 2) {
        tmp = distArr[i]
        distArr[i] = distArr[distArr.size - 1 - i]
        distArr[distArr.size - 1 - i] = tmp
    }
}