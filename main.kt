import java.io.BufferedReader
import java.io.File

fun main(args: Array<String>) {
    val bufferedReader: BufferedReader = File("./input.txt").bufferedReader()
    val inputString = bufferedReader.use { it.readText() }
    val stringsArray = inputString.split("\n")
    val n = stringsArray[0].toInt()

    val step = "* * *"
    var steps = 0

    if (n > 0 && n <= 30) {
        println('*')
        for (i in 0 until n) {
            steps = i * 4
            val tmp = CharArray(steps)

            for (k in 0 until steps) {
                tmp[k] = ' '
            }
            val placeholder = String(tmp)
            println("$placeholder*")
            println(placeholder + step)
        }
    }}
