import java.io.BufferedReader
import java.io.File

fun main(args: Array<String>) {
    val bufferedReader: BufferedReader = File("./input.txt").bufferedReader()
    val inputString = bufferedReader.use { it.readText() }

    val stringsArray = inputString.split("\n")
    val N = stringsArray[0].toInt()

    if (N >= 1 && N <= 15) {
        var result = 0.0;
        for (i in N downTo 1 step 1) {
            result = Math.sqrt(3.0*i + result)
            println(result)
        }
        File("output.txt").printWriter().use { out ->
            out.print("${ Math.round(result * Math.pow(10.0,10.0)) / Math.pow(10.0,10.0)} ")
        }
    }
}
